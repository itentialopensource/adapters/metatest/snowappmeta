# Itential ServiceNow Application

## Table of Contents

Some of the page links in this document and links to other GitLab files do not work in Confluence however, the information is available in other sections of the Confluence material.

- [Overview](#overview)
- [Versioning](#versioning)
- [Supported ServiceNow Versions](#supported-servicenow-versions)
- [Supported IAP Versions](#supported-iap-versions)
- [Getting Started](#getting-started)
  - [Helpful Background Information](#helpful-background-information)
  - [Prerequisites](#prerequisites)
  - [How to Install](https://docs.itential.com/opensource/docs/installing-the-itential-automation-services-application)
  - [Testing](#testing)
- [Configuration](#configuration)
- [Using the Itential ServiceNow App](https://docs.itential.com/opensource/docs/how-to-trigger-an-iap-automation-using-itential-app)
- [Additional Information](#additional-information)
  - [Enhancements](#enhancements)
  - [Contributing](#contributing)
  - [Helpful Links](#helpful-links)
  - [Node Scripts](#node-scripts)
- [Troubleshoot](https://docs.itential.com/opensource/docs/faqs)
- [License and Maintainers](#license-and-maintainers)
- [Product License](#product-license)

## Overview

The Itential ServiceNow Application is available to make it easier for our customers to integrate from ServiceNow to Itential Automation Platform. It includes an application that is installed in ServiceNow that can trigger automations in a customers IAPs. It also includes ServiceNow Actions and Flows that can be defined on ServiceNow processes like Change Management or Requests.

## Versioning

The current version of the Itential ServiceNow App can be found in the ServiceNow Store: <a href="https://store.servicenow.com/sn_appstore_store.do#!/store/application/f2ba728813da041032813092e144b01f" target="_blank">Itential ServiceNow App</a>.

## Supported ServiceNow Versions

The Itential ServiceNow App works with the Tokyo, Utah and Vancouver releases of ServiceNow.

## Supported IAP Versions

The Itential ServiceNow App works with the 2021.1.x, 2021.2.x, 2022.1.x and 2023.1.x of the Itential Automation Platform.

## Getting Started

These instructions will help you download the Itential ServiceNow App from the ServiceNow store and install it into your ServiceNow instance. Reading this section is also helpful for deployments as it provides you with pertinent information on prerequisites and properties.

### Helpful Background Information

There is documentation available on the Itential Documentation Site <a href="https://docs.itential.com/opensource/docs/servicenow-application" target="_blank">HERE</a>. This documentation includes information and examples that are helpful for:

```text
Installing
Setting Up Roles
Setting Up Permissions
Configuring IAP
Triggering an IAP Automation
Frequently Asked Questions
```

### Prerequisites

The only pre-requisite is that your instance of ServiceNow has access to the Internet. This is so that it can retrieve the Itential ServiceNow Application from the store, be able to import the various libraries that are needed and so that it can get to the Itential Automation Platform instances you are trying to integrate with.

### [How to Install](https://docs.itential.com/opensource/docs/installing-the-itential-automation-services-application)

### Testing

Mocha is generally used to test all Itential Opensource Adapters. There are unit tests as well as integration tests performed. Integration tests can generally be run as standalone using mock data and running the adapter in stub mode, or as integrated. When running integrated, every effort is made to prevent environmental failures, however there is still a possibility.

#### Unit Testing

Unit Testing is done by Itential prior to making the Itential ServiceNow App available in the ServiceNow store. You should start your experience with the Itential ServiceNow App in a development or test instance of both IAP and ServiceNow.

After you install the Itential ServiceNow App into your instance of ServiceNow, you should be able to follow the instructions to:

* [Setting Up Roles](https://docs.itential.com/opensource/docs/setting-up-roles)
* [Setting Up Permissions](https://docs.itential.com/opensource/docs/setting-up-permissions)
* [Configuring IAP](https://docs.itential.com/opensource/docs/configuring-an-iap-instance)

#### Integration Testing

Once you have configured the Itential ServiceNow App you should be able to see your IAP instance and the available automations in the Itential ServiceNow App. This means your integration to IAP is working. If you do not see the IAP Automations, you should make sure the service account you are using in ServiceNow has permissions to see the IAP Automations within IAP.

Once you can see the automations, follow these instructions for testing that the integration works [Triggering an IAP Automation](https://docs.itential.com/opensource/docs/how-to-trigger-an-iap-automation-using-itential-app)

## Configuration
For every instance of ServiceNow and every instance of IAP that you are integrating follow the installation and configuration process <a href="https://docs.itential.com/opensource/docs/servicenow-application" target="_blank">HERE</a>.

## [Using the Itential ServiceNow App](https://docs.itential.com/opensource/docs/how-to-trigger-an-iap-automation-using-itential-app)

### Authentication

Authentication from ServiceNow to IAP is based on the Service Account information you provide within the Itential ServiceNow App when [Configuring IAP](https://docs.itential.com/opensource/docs/configuring-an-iap-instance). Itential is working on improving the authentication process including the ability to use Single Sign On to IAP.

## Additional Information

### Enhancements

You can request enhancements to the Itential ServiceNow App through the [Itential Service Desk](https://itential.atlassian.net/servicedesk/customer/portals) or through your Itential Customer Success Team Advocate

### Contributing

At the current time the Itential ServiceNow App is a private repository and thus the ability to contribute is difficult and undocumented. If you are interested in contributing to the effort, please not your interest on any enhancement requests.

### Helpful Links

<a href="https://docs.itential.com/opensource/docs/servicenow-application" target="_blank">Itential ServiceNow App</a>.

### Node Scripts

There are no scripts pacakged or required for the Itential ServiceNow App.

## [Troubleshoot](https://docs.itential.com/opensource/docs/faqs)

## License and Maintainers

```text
Itential Ecosystem Applications are maintained by the Itential Product Team.
```

## Product License

Itential ServiceNow Application is available and Licensed through the ServiceNow Store.
